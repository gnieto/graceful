use std::{
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    time::Duration,
};

use futures::{
    channel::mpsc,
    future::{BoxFuture, Fuse},
    select, FutureExt, StreamExt,
};

#[derive(Clone)]
pub struct BackgroundHandler {
    shutdown_tx: mpsc::Sender<()>,
    cancel_tx: tokio::sync::broadcast::Sender<()>,
    closed: Arc<AtomicBool>,
}

impl BackgroundHandler {
    pub fn acquire(&self) -> Option<Link> {
        match self.closed.load(Ordering::SeqCst) {
            true => None,
            false => Some(Link::new(&self.cancel_tx, self.shutdown_tx.clone())),
        }
    }

    fn close(&mut self) {
        // TODO: Review if SeqCST is the best ordering for the current use case
        self.closed.store(true, Ordering::SeqCst);

        // Disconnect the sender, otherwise the `shutdown_rx` will never early return, as we will still
        // hold a Sender on this structure.
        self.shutdown_tx.disconnect();
    }
}

pub struct Background {
    shutdown_rx: mpsc::Receiver<()>,
    handler: BackgroundHandler,
}

impl Default for Background {
    fn default() -> Self {
        let (tx, rx) = mpsc::channel::<()>(1);
        let (broadcast_tx, _) = tokio::sync::broadcast::channel::<()>(1);

        Background {
            shutdown_rx: rx,
            handler: BackgroundHandler {
                shutdown_tx: tx,
                cancel_tx: broadcast_tx,
                closed: Arc::new(AtomicBool::default()),
            },
        }
    }
}

impl Background {
    pub fn acquire(&self) -> Option<Link> {
        self.handler.acquire()
    }

    pub fn handler(&self) -> BackgroundHandler {
        self.handler.clone()
    }

    pub async fn shutdown(mut self, duration: Duration) -> Shutdown {
        self.handler.close();

        // Change the value on the spmc channel to signal all the running futures to stop
        tracing::info!("Broadcasting close");
        if let Err(e) = self.handler.cancel_tx.send(()) {
            tracing::error!("Error sending cancel background jobs: {:?}", e);
            return Shutdown::Error;
        }

        let mut shutdown = self.shutdown_rx.into_future().fuse();
        let mut timer = Self::timer(duration).boxed().fuse();

        // Wait for the first future to resolve:
        // - Shutdown channel will resolve once all the links has been dropped. Note that we store a tx
        //   on all links. Once all tx has dropped, the shutdown rx channel will get resolved.
        // - Timer: If tasks do not graceful end before the giving duration, the timer will resolve and we will
        //   return in a Timeout state.
        select! {
            _ = shutdown => Shutdown::Ok,
            _ = timer => Shutdown::Timeout,
        }
    }

    pub async fn timer(duration: Duration) {
        tokio::time::sleep(duration).await
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum Shutdown {
    Timeout,
    Ok,
    Error,
}

pub struct Link {
    /// `cancel_rx` can be used to select wait for a closing signal. When `Background::shutdown` is
    /// called, a new value is emitted through this spmc channel and will signal to all the running
    /// futures that they need to be stopped.
    cancel_rx: tokio::sync::broadcast::Receiver<()>,

    /// Store a `Sender` for the mpsc channel. When the future is dropped, this struct will
    /// be dropped. When all `Sender`s has been dropped, the channel is closed and it will
    /// early return on `shutdown` future select.
    #[allow(unused)]
    shutdown_tx: futures::channel::mpsc::Sender<()>,
}

impl Link {
    pub fn new(
        cancel_tx: &tokio::sync::broadcast::Sender<()>,
        shutdown_tx: futures::channel::mpsc::Sender<()>,
    ) -> Self {
        Link {
            cancel_rx: cancel_tx.subscribe(),
            shutdown_tx,
        }
    }

    pub async fn wait_for_cancel(&mut self) {
        let _ = self.cancel_rx.recv().await;
    }

    pub async fn wait_select<T>(&mut self, mut f: Fuse<BoxFuture<'_, T>>) -> WaitResult<T> {
        let mut close = self.wait_for_cancel().boxed().fuse();

        select! {
            o = f => WaitResult::Result(o), // If we hit the delay, lets loop again and find for new events
            _ = close => WaitResult::Stop,
        }
    }

    pub async fn wait_sleep(&mut self, duration: std::time::Duration) -> WaitResult<()> {
        let mut close = self.wait_for_cancel().boxed().fuse();
        let mut timer = tokio::time::sleep(duration).boxed().fuse();

        select! {
            _ = timer => WaitResult::Result(()), // If we hit the delay, lets loop again and find for new events
            _ = close => WaitResult::Stop,
        }
    }
}

pub enum WaitResult<O> {
    Stop,
    Result(O),
}

#[cfg(test)]
mod tests {
    use std::time::Duration;

    use futures::{select, FutureExt};

    use crate::{Background, BackgroundHandler, Link, Shutdown};

    #[tokio::test]
    async fn it_wait_for_background_jobs_with_empty_pool() {
        let background = Background::default();
        let status = background.shutdown(Duration::from_secs(2)).await;

        assert_eq!(Shutdown::Error, status);
    }

    #[tokio::test]
    async fn it_timeouts_if_a_future_is_running_on_background_and_it_does_not_cancel() {
        let background = Background::default();
        let tx = background.acquire().unwrap();
        tokio::spawn(async move {
            let _ = tx;
            tokio::time::sleep(Duration::from_secs(10)).await;
        });

        let status = background.shutdown(Duration::from_secs(2)).await;

        assert_eq!(Shutdown::Timeout, status);
    }

    #[tokio::test]
    async fn it_properly_cancels_all_background_jobs_when_signaled() {
        let background = Background::default();
        let mut link = background.acquire().unwrap();
        tokio::spawn(async move {
            let mut sleep = tokio::time::sleep(Duration::from_secs(10)).boxed().fuse();
            let mut cancel = link.wait_for_cancel().boxed().fuse();

            select! {
                _ = sleep => {
                    tracing::error!("Sleep!");

                },
                _ = cancel => {
                    tracing::error!("Cancel!");

                },
            }
        });

        let status = background.shutdown(Duration::from_secs(2)).await;

        assert_eq!(Shutdown::Ok, status);
    }

    #[tokio::test]
    async fn it_timeouts_with_3_fast_futures_and_1_slow_future() {
        let background = Background::default();
        let link = background.acquire().unwrap();
        tokio::spawn(fast_cancel(link));
        tokio::spawn(fast_cancel(background.acquire().unwrap()));
        tokio::spawn(fast_cancel(background.acquire().unwrap()));
        tokio::spawn(slow_cancel(background.acquire().unwrap()));

        let status = background.shutdown(Duration::from_secs(2)).await;

        assert_eq!(Shutdown::Timeout, status);
    }

    #[tokio::test]
    async fn it_finishes_with_3_fast_futures_and_1_slow_future_with_enough_duration() {
        let background = Background::default();
        let link = background.acquire().unwrap();
        tokio::spawn(fast_cancel(link));
        tokio::spawn(fast_cancel(background.acquire().unwrap()));
        tokio::spawn(fast_cancel(background.acquire().unwrap()));
        tokio::spawn(slow_cancel(background.acquire().unwrap()));

        let status = background.shutdown(Duration::from_secs(6)).await;

        assert_eq!(Shutdown::Ok, status);
    }

    #[tokio::test]
    async fn it_finishes_with_fast_nested_futures() {
        let background = Background::default();
        let handler = background.handler();
        let mut rx = nested(handler).await;
        let _ = rx.recv().await;

        let status = background.shutdown(Duration::from_secs(6)).await;

        assert_eq!(Shutdown::Ok, status);
    }

    #[tokio::test]
    async fn it_timeouts_with_a_task_which_does_not_respect_close_signal() {
        let background = Background::default();
        let mut rx = spawn_task_no_cancel(background.handler()).await;
        let _ = rx.recv().await;

        let status = background.shutdown(Duration::from_secs(3)).await;

        assert_eq!(Shutdown::Timeout, status);
    }

    #[tokio::test]
    async fn it_does_not_spawn_if_bakcground_is_in_shutdown_process() {
        let background = Background::default();
        let handler = background.handler();
        let status = background.shutdown(Duration::from_secs(3)).await;

        assert!(handler.acquire().is_none());
        assert_eq!(Shutdown::Error, status);
    }

    async fn fast_cancel(mut link: Link) {
        let mut sleep = tokio::time::sleep(Duration::from_secs(10)).boxed().fuse();
        let mut cancel = link.wait_for_cancel().boxed().fuse();

        select! {
            _ = sleep => println!("Sleep"),
            _ = cancel => println!("Cancel"),
        }
    }

    async fn spawn_task_no_cancel(handler: BackgroundHandler) -> tokio::sync::mpsc::Receiver<()> {
        let (tx, rx) = tokio::sync::mpsc::channel::<()>(1);
        let h1 = handler;

        tokio::spawn(async move {
            tokio::spawn(ignore_cancel(h1.acquire().unwrap()));
            tokio::time::sleep(Duration::from_secs(1)).await;

            tx.send(()).await
        });

        rx
    }

    async fn nested(handler: BackgroundHandler) -> tokio::sync::mpsc::Receiver<()> {
        let (tx, rx) = tokio::sync::mpsc::channel::<()>(1);
        let h1 = handler.clone();
        let h2 = handler.clone();

        tokio::spawn(async move {
            tokio::spawn(fast_cancel(h1.acquire().unwrap()));
            tokio::spawn(fast_cancel(h1.acquire().unwrap()));
            tokio::spawn(fast_cancel(h1.acquire().unwrap()));

            tokio::time::sleep(Duration::from_secs(1)).await;

            tx.send(()).await
        });

        fast_cancel(h2.acquire().unwrap()).await;

        rx
    }

    async fn ignore_cancel(_: Link) {
        tokio::time::sleep(Duration::from_secs(15)).await;
    }

    async fn slow_cancel(_: Link) {
        tokio::time::sleep(Duration::from_secs(4)).await;
    }
}
